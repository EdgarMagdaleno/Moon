const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express');
const favicon = require('serve-favicon');
const app = express();
const http_app = express();
const iplocation = require('iplocation');
const morgan = require('morgan');
const moment = require('moment-timezone');
const nodemailer = require('nodemailer');
const path = require('path')

const transporter = nodemailer.createTransport({
	service: process.env.MOON_BOT_SERVICE,
	auth: {
		user: process.env.MOON_BOT_USER,
		pass: process.env.MOON_BOT_PASSWORD
	}
});

const mail_options = {
	from: '"Cosmo" <' + process.env.MOON_BOT_USER + '>',
	to: 'edgarmv97@gmail.com'
};

const server_options = {
	ca: fs.readFileSync('certs/chain.pem'),
	key: fs.readFileSync('certs/privkey.pem'),
	cert: fs.readFileSync('certs/cert.pem')
};

const projects = [
	'Zen.html',
	'Void.html',
	'Zoon.html',
	'Bismarck.html',
	'HaxePunk.html',
	'Light.html',
	'Puyo.html',
	'Life.html'
];

const padding = Array(256).join(' ');
pad = function(amount, string) {
	return (string + padding).substring(0, amount);
};
/*
app.use(morgan(function(tokens, req, res) {
	log(req);
}, {
	skip: function(req, res) {
		return req.url.length > 20 || req.url == process.env.MOON_LOGS || req.url == "/favicon.ico";
	}
}));
*/
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use("/", express.static(path.join(__dirname, 'public')));

app.engine('.html', require('ejs').__express);
app.set('views', __dirname + '/views');
app.set('view engine', 'html');
/*
log = function(req) {
	let ip = req.connection.remoteAddress.substr(7);
	let http_req = pad(24, req.method + ' ' + req.url);
	let timestamp = pad(40, moment().tz('America/Tijuana').format('LLLL'));

	iplocation(ip, function (err, res) {
		fs.appendFile('connections.log', timestamp + '\t' + ip + '\t\t' + http_req + '\t\t' + res.city + ', ' + res.country_name + '\n');

		if(req.url == '/media/void.png') {
			mail_options.subject = 'Someone in ' + res.city + ', ' + res.country_name + ' visited your website.';
			transporter.sendMail(mail_options, function(err) {
				if(err) {
					console.error(err);
				}
			});
		}
	});
};
*/
http_app.get('*', function(req, res) {
	res.redirect('https://' + req.get('host') + req.url);
});

app.get('/', function(req, res) {
	res.render('pages/portfolio', {
		projects: projects
	});
});

app.get('/resume', function(req, res) {
	res.render('pages/resume');
});

app.get('/server_logs', function(req, res) {
	res.sendFile(__dirname + '/logs.txt');
});

app.get('project/:project_name', function(req, res) {
	respond.render('projects/' + project_name);
});

app.get('/applications/:application_name', function(req, res) {
	res.sendFile(__dirname + '/resources/cursors/index.html');
});

http.createServer(http_app).listen(process.env.MOON_HTTP_PORT || 80);
https.createServer(server_options, app).listen(process.env.MOON_HTTPS_PORT || 443);